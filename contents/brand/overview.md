---
name: Overview
---

<img class="gl-display-block gl-mx-auto gl-mb-7 img-25" src="/img/brand/gitlab-logo.svg" alt="GitLab logo" role="img" />

These guidelines set the standard for upholding a cohesive brand image. Our brand, like our product and company, has arrived to where it is today through the collaboration of numerous contributors and iterative processes. It exists as the creative expression of our company mission, vision, and [values](https://about.gitlab.com/handbook/values/).

At GitLab, we’ve created an environment where everyone can contribute, even when it comes to our brand. In 2015, the brand first emerged through the collaborative efforts of the wider GitLab community. After our company went public in 2021, we took a closer look at all its components and began a larger iteration to align our image with the next chapter of our journey. With this creative refresh we put the product at the center of our brand and refined each element to work seamlessly together.

Our product propels users into the future of DevOps. With that purpose in mind, we’ve designed our brand to embody the efficiency, innovation, modern-edge, and simplicity of our single application DevSecOps Platform.

## Brand principles

Our designs are inspired by four key principles that allow us to create a multitude of visuals that maintain a consistent and cohesive brand identity. These brand principles embody the personality of our brand and are actuated by our product; they should be equally balanced when designing, each working together to tell an intentional story.

- **Human** - We are a people-first company with a community-centric product. We strive for open and approachable designs that prioritize inclusivity, accessibility, and transparency.
- **Reliable** - We maintain a consistent brand image that reinforces our trustworthiness and echoes the efficiency and productivity our product and company are known for.
- **Dynamic** - We are risk-takers that have paved the way with a comprehensive DevSecOps Platform. We create modern and innovative designs where multiple pieces work together to tell a meaningful story.
- **Clear** - We streamline the complex and show intricate concepts in a minimal way. We aim for simplified designs and messaging that are confident, clean, and informative, keeping many audiences in mind.

## Tone of voice

Our voice embodies our [values](https://about.gitlab.com/handbook/values/) and [mission](https://about.gitlab.com/company/mission/#mission). We openly welcome others into our collaborative space, sharing an inclusive perspective on GitLab’s vision.

Our brand principles extend to our voice and [communications](https://about.gitlab.com/handbook/communication/#writing-style-guidelines) as well. Consider how each characteristic works together to create a holistic narrative. For instance, our human-centric approach balances our ambitious edge; our concise messaging reinforces our trustworthiness; and so on. All facets are needed to fully represent who we are. Depending on the audience, it may make sense to lean into one area more than another.

| **Sample headline** | **Approach** |
| ------ | ------ |
| _The DevSecOps Platform grows with you._ | **Human**. We invite others because we value inclusivity and transparency. We are approachable, relatable, and collaborative. |
| _Powerful, scalable, end-to-end automation is certain with GitLab._ | **Reliable**. We provide others with trustworthy and knowledgeable resources. We are helpful, confident, and consistent. |
| _A community of big ideas for better DevSecOps._ | **Dynamic**. We are dedicated to positive change. We are iterative, clever, and curious. |
| _GitLab. The DevSecOps Platform._ | **Clear**. We are concise and straight to the point. We are efficient, informative, and organized. |

## Resources

**Logo**

- [Press kit](https://about.gitlab.com/press/press-kit/)

**Color**

- [RGB swatches](https://gitlab.com/gitlab-com/marketing/brand-product-marketing/brand-product-marketing/brand-design/-/blob/e6e2bb24e899078935d1aeb0e65c226b6bf36a8b/brand/brand-assets/brand-color-palettes/gitlab-rgb.ase) (Internal access only)
- [CMYK swatches](https://gitlab.com/gitlab-com/marketing/brand-product-marketing/brand-product-marketing/brand-design/-/blob/e6e2bb24e899078935d1aeb0e65c226b6bf36a8b/brand/brand-assets/brand-color-palettes/gitlab-cmyk-pms.ase) (Internal access only)

**Marketing illustrations**

- [Illustration library](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/tree/master/design/_resources/_designer-resources/assets/illustration-library) (Internal access only)
- [RGB swatches, secondary palette](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/blob/master/design/_resources/_designer-resources/color-palettes/gitlab-secondary-color-palette-hex-rgb.ase) (Internal access only)
- [CMYK swatches - secondary palette](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/blob/master/design/_resources/_designer-resources/color-palettes/gitlab-secondary-color-palette-pms-cmyk.ase) (Internal access only)

**Marketing icons**

- [Icon library](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/tree/master/design/_resources/_designer-resources/assets/icon-library/marketing-icons) (Internal access only)
- [Software development lifecycle](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/tree/master/design/_resources/_designer-resources/assets/icon-library/software-development-lifecycle-icons) (Internal access only)

**Diagrams**

- [Diagram library](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/tree/master/design/_resources/_designer-resources/assets/diagrams) (Internal access only)

**Photography**

- [Photo library](https://drive.google.com/drive/folders/1VHErs-KSNX1FIIVgXJR3OmIzwU7M4E1M?usp=sharing) (Internal access only)
- [Adobe stock](https://stock.adobe.com/)

**Templates**

- [General GitLab-themed deck template](https://docs.google.com/presentation/d/1xuw2RrjoSPx69p9v7aacrustmVto8cKWaFn_YK7Riug/edit#slide=id.g1287bf62b57_0_209) with [instructions](https://about.gitlab.com/handbook/tools-and-tips/#google-slides)
- [GitLab pitch deck template](https://docs.google.com/presentation/d/1vtFnh8DU6ZZzASTHg83UrhM6LJWqo5lq9mJDAY2ILpw/edit?usp=sharing)

**Quick links**

- [Corporate Marketing handbook](https://about.gitlab.com/handbook/marketing/corporate-marketing/)
- [Brand Design handbook](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-design/)
