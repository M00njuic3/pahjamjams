---
name: Marketing illustrations
---

<figure-img alt="GitLab illustration banner sample" label="Illustration sample" src="/img/brand/marketing-illustrations.png"></figure-img>

[Illustrations](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/tree/master/design/_resources/_designer-resources/assets/illustration-library) are the primary visual tool that we use throughout our marketing materials to depict GitLab’s values, features, user outcomes, and abstract concepts. The illustrations are an extension of our unique iconography system, and both reinforce our brand personality and are inspired by our values in the following ways:

- Overlapping elements with overlays, which highlight collaboration and transparency
- Intentional color application, driven by results
- Motion and dashed lines that represent efficiency and iteration

### Construction

An illustration typically accompanies copy; the two together are necessary and should balance and complement one another. An illustration should elevate the message and act as a form of brand storytelling, adding clarity and context to complex ideas.

When building illustrations, use this [template](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/blob/master/design/_resources/_designer-resources/assets/illustration-library/_artwork/gitlab-illustration-source-file-1.7.ai) and adhere to the following guidelines to maintain visual consistency:

1. **Solid-filled shapes and Charcoal linework** are combined and balanced throughout.
1. **Shapes** add weight to the piece and create the overall composition of the illustration.
   1. A single shape can also be used as a background element to ground the visual. In this case, the illustration should break out of the container.
1. **Lines** are used for key areas of focus in the piece.
   1. Lines are 2px in weight.
1. **Detail** should be minimal, but added for context.
   1. Secondary lines that depict motion use Purple 04s.
1. **Backgrounds** are typically white or a solid shape filled with Purple 02s.
   1. If working with a dark background, the illustration should be placed on a gradient-filled circle that provides ample contrast for the illustration.
   1. The background shape should have two corners with a border radius of 150px.
1. **Corners** use a mix of angles and curves. Rounded corners are used sparingly as a small addition of personality; the radius should be consistent throughout the illustration.
1. **Color** is used intentionally, with white and purples from the [secondary palette](https://drive.google.com/file/d/1kCcvxYMKPkDCEFQd6imQcHhFGC14Hgte/view?usp=sharing) making up the majority of the composition. The remaining colors in that palette are used as highlights and accents to differentiate objects within the illustration.

<figure-img alt="GitLab illustration sample" label="Illustration sample" src="/img/brand/illustrations.svg" width="480"></figure-img>

## Marketing icons

[Marketing icons](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/tree/master/design/_resources/_designer-resources/assets/icon-library/marketing-icons) are the building blocks of our illustration style and are used as singular units. They should be straightforward, only visualizing a single topic or call to action. Icons are quickly identifiable, representing common items in a redacted way. They take a secondary role to the copy and are used at small scales ranging from 24px - 216px; anything larger should default to an illustration and anything intended for functional use within the product should use the [Product icon set](/product-foundations/iconography) instead.

### Construction

The marketing icons are created using the 8px grid system found within this [template](https://drive.google.com/file/d/1V-FdsDeYcx_yPBMI9cjWclWAz_TPYqFU/view?usp=sharing). They consist entirely of linework, using a weight of 2px (1px for icons smaller than 64px) and Charcoal color. Alternatively, white is used if the icon is placed on a dark background. The icons should contain one rounded corner, if possible with the shape, with a radius of 12px (3px for icons smaller than 64px).

### Software development lifecycle

[This specific set of icons](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/tree/master/design/_resources/_designer-resources/assets/icon-library/software-development-lifecycle-icons) represents each stage of our software development lifecycle at each stage of the process. They are designed to be presented as a set when referring to the entire journey of the lifecycle or individually when referring to a specific stage.

<figure-img alt="10 icons, one for each development stage" label="Software development lifecycle icon set" src="/img/brand/sdlc-icon-set.svg"></figure-img>

## Marketing diagrams

[Diagrams](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/tree/master/design/_resources/_designer-resources/assets/diagrams) are used to illustrate the functionality of the GitLab product. They act like a manual, representing complex ideas step-by-step. They should be structured and visually clear, while using as minimal detail as possible.

### Color

- Diagrams use plenty of white space to make the story easy to follow and understand.
- Orange 03p or Teal 02s can be used sparingly on icons to denote a positive or negative association. **Example:** Orange 03p for an X, or Teal 02s for a check mark.
- With white backgrounds, use Charcoal for the linework and type.
  - Gray 03 can be used for dashed lines and motion lines
  - Purple 01s can be used to add depth to elements or as an extra layer of color in the background.
- With Charcoal backgrounds, use white for the linework and type.
  - Gray 03 can be used for dashed lines and motion lines.
  - Purple 01s can be used in parts of the linework to highlight and/or direct the user’s eye through the flow of the diagram.
  - Gray 05 can be used to add depth to elements or used as an extra layer of color in the background.

### Linework

- Motion lines are used as accents; they should help tell the story and/or guide the user’s eye through the diagram’s flow.
- Line breaks give the feel of openness throughout the diagram. Lines should break before touching an icon, unless the icon is contained within a circle.
- Dashed lines are used to connect type and visual elements.

### Marketing icons

- Lines should break before touching an icon, unless the icon is contained within a circle.
- When using an icon within a circle, ensure the icon has 25% padding around it.

### Logo

- A single-color tanuki (either Charcoal or Orange 02p) can be used sparingly in diagrams to denote a product feature.
- When representing how the GitLab product works in conjunction with or compared to another product, use the core logo.

<figure-img alt="A light and dark background version of a diagram detailing the DevOps lifecycle journey" label="Diagram sample" src="/img/brand/diagram.svg"></figure-img>
